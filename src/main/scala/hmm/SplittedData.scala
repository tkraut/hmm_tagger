package hmm

/**
 * Created by tomas on 30.5.15.
 */
case class SplittedData(training: Data, heldout: Data, testing: Data)
