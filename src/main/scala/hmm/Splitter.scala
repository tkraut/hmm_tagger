package hmm


/**
 * Created by tomas on 30.5.15.
 */
trait Splitter {
  def join(t: Data, h: Data, s: Data): Data

  def split(data: Data): SplittedData
}

object Splitter {
  final val TESTING_SIZE = 40000
  final val HELDOUT_SIZE = 20000
  lazy val test = new Splitter {
    override def split(data: Data): SplittedData = SplittedData(data take 5, data.slice(5, 8), data drop 8)
    override def join(t: Data, h: Data, s: Data): Data = t ++ h ++ s
  }
  /**
   * T, H, S
   */
  lazy val THS = new Splitter {
    override def split(data: Data) = SplittedData(data dropRight TESTING_SIZE + HELDOUT_SIZE, data dropRight TESTING_SIZE takeRight HELDOUT_SIZE, data takeRight TESTING_SIZE)
    override def join(t: Data, h: Data, s: Data): Data = t ++ h ++ s
  }
  /**
   * S, H, T
   */
  lazy val SHT = new Splitter {
    override def split(data: Data) = SplittedData(data drop TESTING_SIZE + HELDOUT_SIZE, data drop TESTING_SIZE take HELDOUT_SIZE, data take TESTING_SIZE)
    override def join(t: Data, h: Data, s: Data): Data = s ++ h ++ t
  }
  /**
   * H, S, T
   */
  lazy val HST = new Splitter {
    override def split(data: Data) = SplittedData(data drop TESTING_SIZE + HELDOUT_SIZE, data take HELDOUT_SIZE, data drop HELDOUT_SIZE take TESTING_SIZE)
    override def join(t: Data, h: Data, s: Data): Data = h ++ s ++ t
  }
  /**
   * S, T, H
   */
  lazy val STH = new Splitter {
    override def split(data: Data) = SplittedData(data drop TESTING_SIZE dropRight HELDOUT_SIZE, data takeRight HELDOUT_SIZE, data take TESTING_SIZE)
    override def join(t: Data, h: Data, s: Data): Data = s ++ t ++ h
  }

  /**
   * H, T, S
   */
  lazy val HTS = new Splitter {
    override def split(data: Data) = SplittedData(data drop HELDOUT_SIZE dropRight TESTING_SIZE, data take HELDOUT_SIZE, data takeRight TESTING_SIZE)
    override def join(t: Data, h: Data, s: Data): Data = h ++ t ++ s
  }

  /**
   * T, S, H
   */
  lazy val TSH = new Splitter {
    override def split(data: Data) = SplittedData(data dropRight HELDOUT_SIZE + TESTING_SIZE, data takeRight HELDOUT_SIZE, data dropRight HELDOUT_SIZE takeRight TESTING_SIZE)
    override def join(t: Data, h: Data, s: Data): Data = t ++ s ++ h

  }

  def apply(id: String): Splitter = id match {
    case "THS"|"1" => THS
    case "SHT"|"2" => SHT
    case "STH"|"3" => STH
    case "TSH"|"4" => TSH
    case "HTS"|"5" => HTS
    case "HST" => HST
    case _ => throw new IllegalArgumentException(s"Invalid split id: $id")
  }
}
