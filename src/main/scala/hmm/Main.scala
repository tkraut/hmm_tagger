package hmm

import java.io.File
import java.lang.Math.log

import hmm.Structures.{Word, ZeroTag}

import scala.annotation.tailrec
import scala.util.Try

/**
 * Created by tomas on 30.5.15.
 * Usage: <pre>scala Main [filename] [splitId] [maxN] [name]
 */
object Main extends App {
  val filename :: splitId :: maxNStr :: name :: _ = args.toList
  val data = PtgLoader.load(filename, "iso-8859-2")
  private val splitter = Splitter(splitId)
  val SplittedData(t, h, s) = splitter.split(data)
  assert(h.size == 20000)
  assert(s.size == 40000)
  assert(t.size == data.size - 60000)
  assert(splitter.join(t, h, s) == data)
  val (words, tags) = t.unzip
  val maxN = Try(maxNStr.toInt).getOrElse(3)
  val model = Trainer.train(t, maxN)
  val smoothed = Trainer.smooth(model, h)
  val tagset = tags.toSet + ZeroTag
  val taglist = tagset.toList
  val trellis = new Trellis[NGram, Word] {


    override def prune: Layer => Layer = {
      case Layer(computedStates) => Layer(computedStates sortWith (_.prob > _.prob) take 50)
    }

    override implicit def add(a: Double, b: Double): Double = a + b

    override implicit val zero: Double = 0.0

    override val alphabet: Set[Word] = words.toSet

    override val initial: NGram = (for (_ <- 1 until maxN) yield ZeroTag).toList

    override def emission(from: NGram, to: NGram, what: Word): Option[Double] = smoothed.probability(what, to.head) map log

    override val states: List[NGram] = {
      @tailrec
      def combineAcc(acc: List[NGram], count: Int): List[NGram] = {
        if (count == 0) acc
        else
          combineAcc(for (v <- acc; tag <- tagset) yield tag :: v, count - 1)
      }
      combineAcc(List(Nil), maxN - 1)
    }

    override def transitions(from:NGram): List[NGram] = {
      val right = from.dropRight(1)
      taglist.map(t => t :: right)
    }

    def hasTransition(from: NGram, to: NGram): Boolean = to.tail == from.dropRight(1)

    def transitionUnsafe(from: NGram, to: NGram) = smoothed.probability(to.head :: from) map log
  }
  private val testData = s // take 100
  val (tWords, tTags) = testData.unzip
  val matches = for (computed <- trellis.viterbi(tWords)) yield computed map (_.head) zip testData map {
    case (actual, (word, expected)) =>
      actual == expected
  }
  for (m <- matches) {
    val correct = m.count(identity)
    val size = m.size
    val ratio = if (size > 0) correct.toDouble / size else "NA";
    val f = new File(s"$name.out")

    println(s"$name\t$correct\t$size\t$ratio")
  }
}
