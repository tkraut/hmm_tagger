#!/bin/bash
for file in cz*.result; do tail -n2 $file|head -n1; done > results.cz.tsv
for file in en*.result; do tail -n2 $file|head -n1; done > results.en.tsv
