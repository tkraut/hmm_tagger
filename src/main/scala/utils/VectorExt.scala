package utils

/**
 * Created by tomas on 30.5.15.
 */
object VectorExt {
  implicit class VectorExt[T](val orig: Vector[T]) {
    def prepend(what: T, count: Int): Vector[T] = count match {
      case 0 => orig
      case i if i>0 => what +: prepend(what, count-1)
      case _ => throw new IllegalArgumentException(s"count must be positive, but $count given")
    }
  }
}
