package hmm

import hmm.Structures.{Word, Tag}

/**
 * Created by tomas on 30.5.15.
 */
trait Model {
  /**
   * probability of ngram.head given ngram.tail
   * tail values are in reverse order
   * @param ngram
   * @return
   */
  def probability(ngram: NGram): Option[Double]

  /**
   * Probability of word given tag
   * @param word
   * @param tag
   * @return
   */
  def probability(word: Word, tag: Tag): Option[Double]
  val maxN: Int
}
