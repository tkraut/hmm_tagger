package hmm

import java.nio.charset.Charset

import Structures.{Tag, Word}

import scala.io.{Codec, Source}

/**
 * Created by tomas on 30.5.15.
 */
object PtgLoader {
  def load(s: String, enc: String): Data = {
    val lines = Source.fromFile(s)(new Codec(Charset.forName(enc))).getLines()
    val it = lines map {
      case line: String => line.split("/", 2)
    } map {
      case Array(w:String,t:String) => (new Word(w),new Tag(t))
    }
    it.toList
  }

}
