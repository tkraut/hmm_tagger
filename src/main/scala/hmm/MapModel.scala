package hmm

import hmm.MapModel.{EmmissionMap, TransitionMap}
import hmm.Structures.{Word, Tag}

/**
 * Created by tomas on 30.5.15.
 */
class MapModel(val transitions: TransitionMap, emissions: EmmissionMap, val maxN: Int) extends Model {
  def this(trans: TransitionMap, emms: EmmissionMap) = this(trans, emms, trans.keys.map(_.length).max)

  override def probability(ngram: NGram) = transitions get ngram

  override def probability(word: Word, tag: Tag) = emissions get tag map (_(word))
}

object MapModel {
  type TransitionMap = Map[NGram, Double]
  type EmmissionMap = Map[Tag, Map[Word, Double]]
}

