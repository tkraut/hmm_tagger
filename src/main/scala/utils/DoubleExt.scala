package utils

/**
 * Created by tomas on 27.6.15.
 */
object DoubleExt {
  val delta = 0.0000001
  implicit class DoubleExt(val orig: Double) extends AnyVal {
    def closeTo(other: Double): Boolean = Math.abs(orig - other) < delta
  }

}
