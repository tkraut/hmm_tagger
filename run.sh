#!/bin/bash

sbt "run texten2.ptg 3 1 en3-1" > en3-1.result &
sbt "run texten2.ptg 3 2 en3-2" > en3-2.result &
sbt "run texten2.ptg 3 3 en3-3" > en3-3.result &
sbt "run texten2.ptg 3 4 en3-4" > en3-4.result &
sbt "run texten2.ptg 3 5 en3-5" > en3-5.result &

sbt "run textcz2.ptg 3 1 cz3-1" > cz3-1.result &
sbt "run textcz2.ptg 3 2 cz3-2" > cz3-2.result &
sbt "run textcz2.ptg 3 3 cz3-3" > cz3-3.result &
sbt "run textcz2.ptg 3 4 cz3-4" > cz3-4.result &
sbt "run textcz2.ptg 3 5 cz3-5" > cz3-5.result &

wait

cat *.result > results.tsv
