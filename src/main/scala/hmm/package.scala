package object hmm {

  import Structures.{Tag, Word}

  type NGram = List[Tag]

  type Data = List[(Word, Tag)]
}
