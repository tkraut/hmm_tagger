package hmm

import java.time.Duration
import java.time.LocalDateTime.now

import scala.annotation.tailrec
import scala.util.Try

/**
 * Created by tomas on 4.6.15.
 */
trait Trellis[S, O] {
  val alphabet: Set[O]

  val states: List[S]

  def transitions(from:S): List[S] = states.filter(to => hasTransition(from, to))

  def transition(from: S, to: S): Option[Double] = if (hasTransition(from, to)) transitionUnsafe(from, to) else None

  /**
   * Does not check whether transition is present, when it is NOT, result can be arbitrary
   * @param from
   * @param to
   * @return
   */
  def transitionUnsafe(from: S, to:S): Option[Double]

  def hasTransition(from: S, to: S): Boolean

  def emission(from: S, to: S, what: O): Option[Double]

  val initial: S

  def prune: Layer => Layer = identity

  //default multiplying, where zero element is 1
  implicit def add(a: Double, b: Double) = a * b

  implicit val zero = 1.0

  def viterbi(seen: Seq[O]): Option[Seq[S]] = {
    val first = new Layer(List(new ComputedState(initial, zero, None)))
    var i = 1
    val start = now
    var last = start
    val size = seen.size
    val lastLayer = seen.foldLeft(first) { case (layer, output) =>
      if (i % 1000 == 0) {
        val current = now()
        println(s"$i/$size, elapsed: ${Duration.between(last, current)}, total ${Duration.between(start, current)}")
        last = current
      }
      i = i + 1
      val next = layer.next(output)
      if (next.currentStates.isEmpty) println("Empty")
      next
    }
    lastLayer.currentBest.map(_.stateSequence)
  }


  class ComputedState(val s: S, val prob: Double, val prev: Option[ComputedState]) {
    @tailrec
    private def stateSequenceAcc(acc: List[S]): List[S] = prev match {
      case None => acc
      case Some(cs) => cs.stateSequenceAcc(s :: acc)

    }

    def stateSequence: Seq[S] = {
      stateSequenceAcc(Nil)
    }

    lazy val hasPrev = prev.isDefined

    override def toString = s"ComputedState($s, $prob, $hasPrev)"

  }


  case class Layer(currentStates: List[ComputedState]) {
    lazy val currentBest: Option[ComputedState] = Try(currentStates maxBy (_.prob)).toOption

    def next(output: O): Layer = alternativeNext(output)


    def alternativeNext(output: O): Layer = {
      val all = for {
        cs <- currentStates
        state <- transitions(cs.s)
        trans <- transitionUnsafe(cs.s, state)
        em <- emission(cs.s, state, output)
      } yield new ComputedState(state, add(cs.prob, add(trans, em)), Some(cs))
      prune(Layer((all groupBy (_.s) mapValues (_ maxBy (_.prob))).values.toList))
    }

    def oldnext(output: O): Layer = {
      val nextLayer = for (state <- states) yield {
        val all = currentStates flatMap {
          cs =>
            for {
              trans <- transition(cs.s, state)
              em <- emission(cs.s, state, output)
            } yield new ComputedState(state, add(cs.prob, add(trans, em)), Some(cs))
        }
        Try(all maxBy (_.prob)).toOption
      }
      prune(Layer(nextLayer.flatten))
    }

  }

}
