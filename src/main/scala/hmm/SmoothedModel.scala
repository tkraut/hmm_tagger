package hmm

import hmm.Structures.{Word, Tag}

/**
 * Created by tomas on 30.5.15.
 */
class SmoothedModel(val original: Model, val lambdas: Vector[Double]) extends Model {
  if (lambdas.sum - 1d >= SmoothedModel.delta) {
    throw new IllegalArgumentException(s"lambdas should sum up to 1.0, current value is ${lambdas.sum}")
  }
  override def probability(ngram: NGram): Option[Double] = Some(partialProbabilities(ngram).collect { case Some(d) => d }.sum)

  def partialProbabilities(ngram: NGram) = {
    for {
      i <- 0 to ngram.length
    } yield original.probability(ngram.take(i)).map(_ * lambdas(i))
  }

  override val maxN: Int = original.maxN

  override def toString = s"SmoothedModel(lambdas: $lambdas, original: $original)"

  override def probability(word: Word, tag: Tag) = original.probability(word, tag)
}

object SmoothedModel {
  final val delta = 0.000000000001
}
