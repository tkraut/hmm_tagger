#!/bin/bash

sbt "run texten2.ptg 1 3 en1-3" > en1-3.result &
sbt "run texten2.ptg 2 3 en2-3" > en2-3.result &
sbt "run texten2.ptg 4 3 en4-3" > en4-3.result &
sbt "run texten2.ptg 5 3 en5-3" > en5-3.result &

sbt "run textcz2.ptg 1 3 cz1-3" > cz1-3.result &
sbt "run textcz2.ptg 2 3 cz2-3" > cz2-3.result &
sbt "run textcz2.ptg 4 3 cz4-3" > cz4-3.result &
sbt "run textcz2.ptg 5 3 cz5-3" > cz5-3.result &

wait

cat *.result > results.tsv
