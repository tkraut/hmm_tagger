package hmm

/**
 * Created by tomas on 30.5.15.
 */
object Structures {
  case class Word(s: String) extends AnyVal{

  }
  case class Tag(s:String) extends AnyVal{}

  final val OOVWord = Word("^^^")
  final val ZeroTag = Tag("^^^")
}
