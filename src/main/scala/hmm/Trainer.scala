package hmm

import java.lang.Math.abs

import hmm.Structures.{Tag, Word, ZeroTag}

import scala.annotation.tailrec

import utils.DoubleExt.DoubleExt

/**
 * Created by tomas on 30.5.15.
 */
object Trainer {

  def train(data: Data, maxN: Int): Model = {
    val (words, tags) = data.unzip
    val rawEmissionsCount: Map[(Word, Tag), Int] = data.groupBy(identity).mapValues(_.length)
    val tagCounts: Map[Tag, Int] = data.groupBy(_._2).mapValues(_.length)
    val wordCounts: Map[Word, Int] = data.groupBy(_._1).mapValues(_.length)
    val wordSet: Set[Word] = wordCounts.keySet + Structures.OOVWord
    val tagSet: Set[Tag] = tagCounts.keySet
    val tagsetSize = tagSet.size
    val wordsetSize = wordSet.size
    def tagEmission(x: (Tag, Int)): (Tag, Map[Word, Double]) = x match {
      case (tag, tagCount) =>
        val denominator = wordsetSize + tagCount
        val base = for {
          word <- wordSet
          count <- rawEmissionsCount.get(word, tag)
        } yield {
            word -> (1.0 + count) / denominator
          }
        tag -> base.toMap.withDefaultValue(1.0 / denominator)
    }
    val emissionMap = tagCounts map tagEmission

    /**
     * just check for emissionMap probabilities sum up to 1
     */
    tagSet foreach { tag =>
      val probSum = wordSet.toList.map(w => emissionMap(tag)(w)).sum
      assert(probSum.closeTo(1.0), s"Probs for $tag sum up to 1 (or close to 1): $probSum")
    }

    /**
      * tag uniform probability
      */
    val uniform = 1d / tagsetSize
    val zeroCount: Vector[Map[NGram, Int]] = Vector(Map(Nil -> data.length))
    val nonZeroCount = for (n <- 1 to maxN) yield {
      (nZeroes(n) ::: tags).sliding(n).map(_.reverse).toList.groupBy(identity).mapValues(_.length)
    }
    val ngramCounts = zeroCount ++ nonZeroCount
    val ngramProbs = 1 to maxN map {
      n => ngramCounts(n) map {
        case (ngram, count) => ngram -> count.toDouble / ngramCounts(n - 1)(ngram.tail)
      }
    }
    new MapModel(ngramProbs.flatten.toMap.updated(Nil, uniform), emissionMap, maxN)
  }

  private[this] def nZeroes(n: Int): List[Tag] = 1.to(n).map(_ => ZeroTag).toList

  def smooth(original: Model, heldout: Data): SmoothedModel = {
    val delta = 0.00000001
    val data = heldout.map(_._2)
    @tailrec
    def iter(currentLambdas: Vector[Double]): Vector[Double] = {
      val newLambdas = smoothIteration(original, currentLambdas, data)
      val diffs = currentLambdas zip newLambdas map { case (a, b) => abs(a - b) }
      if (diffs.max < delta) newLambdas else iter(newLambdas)
    }
    val lambdas = 0 to original.maxN map (_ => 1.0 / (original.maxN + 1))
    new SmoothedModel(original, iter(lambdas.toVector))
  }

  def smoothIteration(original: Model, currentLambdas: Vector[Double], data: NGram): Vector[Double] = {
    val model = new SmoothedModel(original, currentLambdas)
    val ngrams = (nZeroes(original.maxN - 1) ::: data).sliding(original.maxN).map(_.reverse)
    val probs = for (ngram <- ngrams) yield {
      val p = model.probability(ngram).getOrElse(0.0)
      model.partialProbabilities(ngram) map (_.getOrElse(0.0) / p)
    }
    val partialSums = probs.reduce((a, b) => a zip b map { case (x, y) => x + y })
    partialSums.map(_ / partialSums.sum).toVector
  }

}
